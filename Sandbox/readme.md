My Machine-Learning Journey
---

In this repo, I track my progress in learning machine learning.

The course I am following is [udemy.com/machinelearning](https://www.udemy.com/machinelearning).

## Tools used

- Jupyter Notebook
- Python libraries
  - Pandas
  - Scikit Learning
